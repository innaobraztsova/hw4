<?php

// Задание 7
$getDiscount = function (int $sum) {
    if ($sum >= 1000 && $sum <= 3000 ) {
        $discount = 500;
    } elseif ($sum < 1000) {
        $discount = 100;
    } else {
        $discount = 1000;
    }
    return $discount;
};

function buyProduct(int $count, int $price, callable $getDiscount)
{
    $total = $count * $price;
    $discount = $getDiscount($total);
    return $total - $discount;
}

$result = buyProduct(4, 250, $getDiscount);
echo $result;