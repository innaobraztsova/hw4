<?php

// Задание 8
$names = ['Maksym', 'Vladimir', 'Genadiy', 'Evgeniy', 'Leonid', 'Max', 'Ekaterina'];
function sortByLength(bool $max, bool $min)
{
    if ($max == $min) {
        return 0;
    }
    return (strlen($max) > strlen($min) ? -1 : 1);
}

usort($names, 'sortByLength');
var_dump($names);
