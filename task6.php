<?php

// Задание 6.
$users =
    [
        ['name' => 'Inna',
            'city' => 'Odessa',
            'age' => 26,
            'gender' => 'women'
        ]
        ,
        ['name' => 'Gena',
            'city' => 'Kiev',
            'age' => 43,
            'gender' => 'men'
        ],
        ['name' => 'Olga',
            'city' => 'Rovno',
            'age' => 13,
            'gender' => 'women'
        ],
        ['name' => 'Roma',
            'city' => 'Kiev',
            'age' => 18,
            'gender' => 'men'
        ]
    ];

$callback = function (string $gender) {
    switch ($gender) {
        case 'men':
            $result = 'blue';
            break;
        case 'women':
            $result = 'pink';
            break;
        default:
            $result = 'black'; // failed
    }
    return $result;
};

function showUserInformation(array $user, callable $callback)
{
    if (!isset($user['name'], $user['city'], $user['age'], $user['gender'])) {
        echo 'Populate all fields';
        return;
    }

    if (!is_int($user['age'])) {
        echo 'Populate the age field';
        return;
    }

    $color = $callback($user['gender']);

    echo sprintf(
        '<p>Name: %s, City: %s, Age: %d, <span style="color:%s;">Gender: %s</span></p>',
        $user['name'],
        $user['city'],
        $user['age'],
        $color,
        $user['gender'],
    );
}

foreach ($users as $user) {
    showUserInformation($user, $callback);
}