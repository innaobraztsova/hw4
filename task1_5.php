<?php

declare(strict_types=1);
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

// Задание 1.
function aloha(string $name)
{
    echo 'Aloha my dear, ' . $name . '!';
}

aloha('Inna');
echo '<br>';

// Задание 2.
function ola(string $name = 'friend')
{
    echo 'Ola my dear, ' . $name . '! <br>';
}

ola();

//Задание 3
function daysOfWeek(int $dayNumber)
{
    $day = [1 => 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
    echo $day[$dayNumber] . '<br>';
}

daysOfWeek(5);

//Задание 4
//Создайте ф-цию, по рассчету веса по формуле Брокка. Ф-ция должна счтитать вес, в зависимости от аргументов.
//Идеальный вес для мужчин = (рост в сантиметрах – 100) · 1,15.
//
//Идеальный вес для женщин = (рост в сантиметрах – 110) · 1,15.
function brokk(int $height, string $gender)
{
    switch ($gender) {
        case 'men':
            $brokk = ($height - 100) * 1.15;
            break;
        case 'women':
            $brokk = ($height - 110) * 1.15;
            break;
        default:
            $brokk = 0;
    }
    echo 'Идеальный вес: ' . $brokk . ' кг <br>';
}

brokk(178, 'men');

// Задание 5.
$numbers = [1, 500, 100, 777, 999, 322, 7871, 9999, 7];

function maxValue(array $numbers)
{
    $maxValue = 0;
    foreach ($numbers as $number) {
        if ($number > $maxValue) {
            $maxValue = $number;
        }
    }
    return $maxValue;
}

$maximum = maxValue($numbers);
echo 'The highest value of the array is ' . $maximum;

